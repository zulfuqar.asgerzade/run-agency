window.addEventListener('load', () => {    
    setTimeout(() => {
        let loadingContainer = document.querySelector(".loadingContainer");
        loadingContainer.style.animation = "removeElement 0.5s forwards ease-in-out";
        setTimeout(() => {
            document.body.removeChild(loadingContainer);
        }, 700);
    }, 1000);
});

window.onbeforeunload = function () {
    window.scrollTo(0, 0);
}


// COUNTER (TIME)
const second = 1000,
      minute = second * 60,
      hour = minute * 60,
      day = hour * 24;

let countDown = new Date('Dec 31, 2021 00:00:00').getTime(),
    x = setInterval(function() {    

      let now = new Date().getTime(),
          distance = countDown - now;

      document.getElementById('days').innerText = Math.floor(distance / (day)),
      document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
      document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
      document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);

    }, second)


// SEND BTN ANIMATION
let sendBtn = document.querySelector(".sendBtn");
sendBtn.addEventListener('mouseover', (event) => {
    
    let arrowIcon = document.querySelector(".arrowIcon");
    arrowIcon.style.color = "white"
});


sendBtn.addEventListener('mouseleave', (event) => {
    
    let arrowIcon = document.querySelector(".arrowIcon");
    arrowIcon.style.color = "#582BAF";
});


// CHECK DATA
function validateEmail(email) {
    const reGex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    return reGex.test(email);
}

function isEmptyData(list) {

    for (let i = 0; i < list.length; i++) {
        
        if(list[i].value == "") {
            alert("Fill all the blank!");
            return false;
        }
    }

    return true;
}

sendBtn.addEventListener('click', (event) => {
    let formInputs = document.querySelectorAll(".formInput");

    if(isEmptyData(formInputs)) {
        
        if (validateEmail(formInputs[1].value)) {
            alert("Your email has been received!");
            formInputs.forEach(element => {
                element.value = "";
            });
        }
        else {
            alert("Please write correct mail!");
        }
    }
    
});